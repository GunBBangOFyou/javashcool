package Net_test1;

import java.awt.*;							// AWT 툴킷 패키지
import java.awt.event.*;				// 리스너 관련 클래스
import java.io.*;								// 인풋, 아웃풋 스트림 관련 패키지
import java.net.*;							// 소켓 관련 패키지
import java.util.Scanner;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;


class HelloFrame extends JFrame {
	
	public HelloFrame (String title){		// 클래스 생성자 메소드
		super(title);											// 상위 클래스인 Frame 생성자에
																			// title스트링 객체 넘겨주어 title 출력
	}
	public Dimension getPreferredSize(){	// 선호하는 프레임 크기 설정
		return new Dimension(460,300);
	}
}

// 프레임의 종료 버튼을 눌렀을 경우 리를 처리하는 리스너
class ExitListener implements WindowListener{
	public void windowClosing(WindowEvent e){
		System.out.println("bye~");
		System.exit(0);
	}
	// 인터페이스를 구현하면 모든 메소드들도 구현해야 함
	// 나머지 이벤트도 오버라이딩으로 구현하지만, 세부 기술은 하지 않음
	public void windowActivated(WindowEvent e) {}
	public void windowClosed(WindowEvent e) {}
	public void windowDeactivated(WindowEvent e) {}
	public void windowDeiconified(WindowEvent e) {}
	public void windowIconified(WindowEvent e) {}
	public void windowOpened(WindowEvent e) {}
}

// 프로그램 주 클래스
public class Client2_test1{
	Socket s;									// 서버와 연결한 소켓 객체 참조 변수 선언
	DataInputStream dis;			// 서버와 통신할 인풋 스트림 참조 변수
	DataOutputStream dos;			// 서버와 통신한 아웃풋 스트림 참조 변수
	JTextArea memo;						// 메시지 출력창
	JTextField field;					// 에코 메시지 입력 창
	JLabel label;							// 상태 표시 라벨
	JScrollPane scrollPane;
	
	int PORT = 5000;		// 에코 서버 포트 번호
	
	String ServerAddr;
	
	public static void main(String [] args){
		Scanner scn=new Scanner(System.in);
		Client2_test1 client = new Client2_test1();
		client.PORT=scn.nextInt();
		scn.nextLine();
		try{
			
			client.ServerAddr = scn.nextLine();						// 서버 주소를 저장해 놓음
			
		}catch(ArrayIndexOutOfBoundsException e){	// args[0]에 서버 주소가 없을 경우 예외 처리
			System.out.println("Java Echo Server needs port number.");
			System.exit(0);
		}
		client.run();
	}
	
	public void run(){													// 서버 접속하고 화면을 구성하는 메소드
		try{
			s = new Socket(ServerAddr, PORT);			// 서버 주소, 포트번호로 서버에 접속
			dis = new DataInputStream(s.getInputStream());	// 서버와 통신할 스트림 생성
			dos = new DataOutputStream(s.getOutputStream());
		}catch(IOException e){
			memo.append("Can't open Socket\n");
			return;
		}
		
		JFrame MainFrame = new HelloFrame("Echo Client");			// 프레임 생성
		MainFrame.addWindowListener(new ExitListener());		// 프레임에 리스너 연결
	
		// 선호하는 프레임 크기를 적용하는 메소드
		MainFrame.pack();
		
		label = new JLabel("Connection completed!");
		
		//메시지 입출력 창
		memo = new JTextArea();
		//memo.setLineWrap(true);
		memo.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(memo); 
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		field = new JTextField();	
		field.addActionListener(new SendListener());		// 에코 메시지 입력창에 리스너 연결
		field.setColumns(50);														// 입력창 너비를 50개 문자 길이로 지정
		
		MainFrame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		MainFrame.getContentPane().add(label, BorderLayout.NORTH);				// 프레임에 라벨 객체를 위에 삽입
		//MainFrame.getContentPane().add(memo, BorderLayout.CENTER);				// 프레임에 출력창 객체를 중앙에 삽입
		MainFrame.getContentPane().add(field, BorderLayout.SOUTH);				// 프레임에 입력창 객체를 아래에 삽입
		
		MainFrame.setVisible(true);											// 프레임을 보이게 하는 메소드
	}

	// 채팅 메시지 송신 처리 리스너
	class SendListener implements ActionListener{
			public void actionPerformed(ActionEvent ae){
				String recvMsg;
				String sendMsg = field.getText();		// 입력창에 스트링 얻기
				try {
					dos.writeUTF(sendMsg);						// 입력 데이터를 아웃풋 스트림으로 서버로 전송
					dos.flush();											// 아웃풋 스트림에 있는 데이터를 바로 전성.
					
				}catch(IOException e){
					memo.append("Sending error\n");
				}
				memo.append("Client Messsage :");		// 입력창에 스트링을 출력창에 표시
				memo.append(sendMsg + "\n");
				field.setText("");
				
				try{
					recvMsg = dis.readUTF() + '\n';		// 서버에서 온 에코 데이터 읽기
					memo.append("Server Message :");	// 출력창에 에코 데이터 표시
					memo.append(recvMsg + "\n");
				} catch(IOException e)
				{
					memo.append("read error");
				}
		}
	}
}