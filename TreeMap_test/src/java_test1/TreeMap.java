
package java_test1;

public class TreeMap {
	TreeMapNode topNode = null;
	public static Comparable test;
	Comparable chack_Key = null;
	
	public void add(Comparable key, Object value) {  //노드를 추가하는 메서드
		if(topNode == null) {
			topNode = new TreeMapNode(key, value);
			test = key;
		}else {
			topNode.add(key, value);
		}
	}
	
	public void search(Comparable key) {    //해당 키를 검색하고 키값을 출력하는 메서드
		if(topNode==null) {
			System.out.println("not found!");
		}else {
			chack_Key=topNode.find(key);
			if(chack_Key==null)System.out.println("not found!");
			else System.out.println(chack_Key);
		}
	}
	
	public void visitAll() {  //트리를 후위순회하여 저장되어 있는 노드들을 출력해주는 메서드
		if(topNode == null) {
			System.out.println("노드가 없습니다.");
		}else {
			topNode.NodePrint(topNode);
		}
	}
	

}

class TreeMapNode {
	private final static int LESS = 0;
	private final static int GREATER = 1;
	
	private Comparable itsKey; //키를 나타내는 변수
	private Object itsValue;  //키의 값을 나타내는 변수
	private TreeMapNode nodes[] = new TreeMapNode[3];  //0은 LESS하위 노드 1은 GREATER하위노드 2는 상위 노드를 가르킴
	
	public TreeMapNode(Comparable key, Object value) {  //생산자
		itsKey = key;
		itsValue = value;
	}
	
	public void add(Comparable key, Object value) {  //기존 키가 존재하면 키의 값을 수정하고 없다면 추가할 곳을 검색하는 메서드
		if(key.compareTo(itsKey)==0) {
			itsValue = value;
		}else {
			addSubNode(selectSubNode(key),key, value);
		}
	}
	private void addSubNode(int node, Comparable key, Object value) { //노드를 생성할 곳에 노드를 생성하는 메서드
		if(nodes[node]==null) {
			nodes[node] = new TreeMapNode(key, value);
			nodes[node].nodes[2]= this;
		}else {
			nodes[node].add(key, value);
		}
	}
	
	
	public Comparable find(Comparable key) {   //해당하는 키의 값을 반환하는 메서드
		if (key.compareTo(itsKey)==0) {
			return itsKey;
		}
		return FindSubNodeForKey(selectSubNode(key), key);
	}
	private int selectSubNode(Comparable key) {  //키값을 비교하여 방향을 정해주는 메서드
		return (key.compareTo(itsKey)<0) ? LESS : GREATER;
		
	}
	private Comparable FindSubNodeForKey(int node, Comparable key) { //해당 방향의 내용을 확인하고 null이 아니면 키의 값을 반환하는 메서드
		return nodes[node]==null ? null : nodes[node].find(key);
	}
	
	
	
	public void NodePrint(TreeMapNode NowNode) {  //저장되어 있는 노드들을 후위순회하여 출력하는 메서드
		if(NowNode != null) {
			if(NowNode.nodes[0]!=null) {
				NowNode.NodePrint(NowNode.nodes[0]);
				System.out.println(NowNode.nodes[0].itsKey);
			}
			if(NowNode.nodes[1]!=null) {
				NowNode.NodePrint(NowNode.nodes[1]);
				System.out.println(NowNode.nodes[1].itsKey);
			}
			
			if(NowNode.nodes[2]==null) {
				System.out.println(NowNode.itsKey);
			}
		}
	}
}
























