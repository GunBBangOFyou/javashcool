package Net_test2;

import java.io.*;				// 인풋, 아웃풋 스트림 관련 패키지
import java.net.*;	
import java.util.*;// 소켓 관련 패키지
public class Server_test2{
	ServerSocket ss = null;		// 서버 소켓
	Socket s = null;					// 클라이언트 소켓
	DataInputStream dis;			// 클라이언트로 들어오는 데이터 담당 스트림
	DataOutputStream dos;			// 클라이언트로 나가는 데이터 담당 스트림
	
	int PORT = 5000;		// 서버 포트 번호

	public static void main(String [] args){
		Server_test2 server = new Server_test2();
		Thread1 test = new Thread1();
		
		while(true) {
			try{
				if((server.s=server.ss.accept())!=null) {
					System.out.println("연결 완료 !!");
					test.run(server);
				}
			}catch(IOException e){
				System.out.println("Can't make ServerSocket");
				System.exit(0);
			}
			
		}
		
	}
		

	
	// 생성자
	Server_test2(){
		Scanner scn=new Scanner(System.in);
		PORT = scn.nextInt();
		scn.nextLine();
		try{
			ss = new ServerSocket(PORT);	// 포트번호를 넘겨주어 서버 객체 생성
		}catch(IOException e){
			System.out.println("Can't make ServerSocket");
			System.exit(0);
		}
		System.out.println("Java	Server started. Accepting the client...");
	}
	
	
}

class Thread1 extends Thread{

	  //  run() 메서드 오버라이딩

		public void run(Server_test2 server){
			
			String msg;
			try{
																			
				server.dis = new DataInputStream(server.s.getInputStream());		// s 소켓 인풋 스트림 얻기
				server.dos = new DataOutputStream(server.s.getOutputStream());	// s 소켓 아웃풋 스트림 얻기
				
				System.out.println("New Client!");
				
				while(true){
					msg = server.dis.readUTF();														// 클라이언트 메시지 읽기
					System.out.println("Client message: " + msg);		// 콘솔화면에 클라이언트 메시지 표시
					server.dos.writeUTF(msg);															// 클라이언트 메시지 에코 해줌
				}
			}catch(IOException e){							// 클라이언트에서 접속을 끊으면 호출 됨
				System.out.println("IO Exception :" + e.getMessage());
			}
		}
	    //스레드에서 실행할 작업


	}

			
