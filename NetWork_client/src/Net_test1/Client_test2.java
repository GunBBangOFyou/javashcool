package Net_test1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

public class Client_test2 {
	final static int MAXBUFFER = 512;
	public void start() {
		Scanner scn = new Scanner(System.in);
		String IP = new String();
		String port = new String();
		System.out.print("호스트 입력 : ");
		IP = scn.nextLine();
		System.out.print("포트 입력 : ");
		port = scn.nextLine();
		
		byte buffer[]=new byte[MAXBUFFER];
		int Length = 0;
		int port_int = Integer.parseInt(port);
		
		
		try {
			InetAddress inetaddr = InetAddress.getByName(IP);
			
			DatagramSocket Socket = new DatagramSocket();
			DatagramPacket send_packet;
			DatagramPacket recv_packet;
			
			
			while(true) {
				System.out.print("메시지를 입력하시오 :");
				Length = System.in.read(buffer);
				
				send_packet = new DatagramPacket(buffer, buffer.length, inetaddr, port_int);
				Socket.send(send_packet);
				
				recv_packet = new DatagramPacket(buffer, buffer.length);
				System.out.println("메시지 받음 ");
				
				Socket.receive(recv_packet);
				
				String result = new String(buffer, 0, Length);
				System.out.println("보낸 데이터 :"+result);
				
				if(result.indexOf("BYE")>=0)break;
			}
			Socket.close();
			
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
