package Net_test1;

import java.net.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.*;
import java.util.*;

import javax.swing.*;

public class Client_test1 extends JFrame{
	public Client_test1()
    {
		
        super("J프레임 테스트");

        // 크기 지정
        setSize(500,500);
        // 창을 보이게함
        JPanel totalPanel = new JPanel();

        JPanel buttonPanel = new JPanel();
        JButton button1 = new JButton("hello");
        button1.setText("eeieieei");
        buttonPanel.add(button1);
        
        totalPanel.add(buttonPanel);
        
        JPanel buttonPanel2 = new JPanel();
        JButton button4 = new JButton("언녕");
        buttonPanel2.add(button4);
        
        totalPanel.add(buttonPanel2);
        
        add(totalPanel);
    }

	public static void main(String[] args) {
		
		Client_test1 f= new Client_test1();
		f.setVisible(true);
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);

        
	}

}
