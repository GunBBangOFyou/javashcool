package Net_test2;


import java.net.*;
import java.io.*;
import java.util.*;

public class Server_test1 {
	final int MAXBUFFER = 512;
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int arg_port = Integer.parseInt(scn.nextLine());		// 포트 번호를 변수에 저장
		new Server_test1().work(arg_port);
	}
	void work(int arg_port) {
		int port = arg_port;
		byte buffer[] = new byte[MAXBUFFER];
		try{
			DatagramSocket Socket = new DatagramSocket(port);		// 비연결형 서버 소켓 객체 생성
			DatagramPacket recv_packet;
			System.out.println("Running the UDP Echo Server... ");
			
			while (true){
				// 데이터 수신
				recv_packet = new DatagramPacket (buffer, buffer.length);
				Socket.receive (recv_packet);
				
				String sentence = new String(recv_packet.getData()); 
           			String capitalizedSentence = sentence.toUpperCase(); 
         				byte tx_buffer[] = new byte[MAXBUFFER];
           			tx_buffer = capitalizedSentence.getBytes(); 

				// 에코 데이터 생성 및 송신: 송신용 패킷 생성
				DatagramPacket send_packet = new DatagramPacket
			              //(recv_packet.getData(), recv_packet.getLength(),
				       (tx_buffer, recv_packet.getLength(),
			                 recv_packet.getAddress(), recv_packet.getPort());
						
				Socket.send(send_packet);
			}
		}catch(UnknownHostException ex){	// 잘못된 서버 주소로 접근시
			System.out.println("Error in the host address ");
		}catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
		}

	}

}
